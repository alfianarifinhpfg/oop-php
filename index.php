<?php

require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep = new Animal("shaun");

echo "Nama hewan = ".$sheep->name ."<br>"; // "shaun"
echo "Jumlah kaki = ".$sheep->legs ."<br>"; // 4
echo "Berdarah dingin = ".$sheep->cold_blooded ."<br> <br>"; // "no"

$sungokong = new Ape ("kera sakti");

echo "Nama hewan = ".$sungokong->name ."<br>";
echo "Jumlah kaki = ".$sungokong->legs ."<br>";
echo "Berdarah dingin =".$sungokong->cold_blooded ."<br>";
echo "Suara = ".$sungokong->yell ."<br> <br>";

$kodok = new Frog ("buduk");

echo "Nama hewan = ".$kodok->name ."<br>";
echo "Jumlah kaki = ".$kodok->legs ."<br>";
echo "Berdarah dingin = ".$kodok->cold_blooded ."<br>";
echo "Lompat = ".$kodok->jump ."<br>";


// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>